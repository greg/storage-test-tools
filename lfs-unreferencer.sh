#!/bin/bash
set -ex

PROJECT_PATH=( )

for i in "${PROJECT_PATH[@]}"
do
  sudo gitlab-rake gitlab:cleanup:orphan_lfs_file_references PROJECT_PATH="$PROJECT_PATH"
done
