#!/bin/bash
set -e

$DOMAIN=gitlab.com

for i in {1..X}
do
  http --follow DELETE "$DOMAIN/api/v4/projects/$PROJECT_ID" Private-Token:$TOKEN
  sleep 1
done
