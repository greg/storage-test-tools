#!/bin/bash
set -e

REPOSITORY_URL=""
DOMAIN="gitlab.com"
REPOSITORY_NAME=""
PROJECT_ID=""

http -b --follow GET $DOMAIN/api/v4/projects/$PROJECT_ID?statistics=true Private-Token:$TOKEN | jq . | grep -E 'path_with_namespace|http_url_to_repo|size|commit_count' | awk '{print $1, $2}' | tee -a $REPOSITORY_NAME-before.json

git clone --bare $REPOSITORY_URL
cd $REPOSITORY_NAME.git
git for-each-ref
du -h .

# git filter-repo --path README.me
# git filter-repo --strip-blobs-bigger-than 1M
# git filter-repo --path big.txt --invert-path

#  git filter-repo --blob-callback '
#   if blob.data.startswith(b"version https://git-lfs.github.com/spec/v1"):
#     size_in_bytes = int.from_bytes(blob.data[124:], byteorder="big")
#     if size_in_bytes > 10*1000:
#       blob.skip()
#   '

du -h .
git for-each-ref

http -b --follow DELETE $DOMAIN/api/v4/projects/$PROJECT_ID/protected_branches/master Private-Token:$TOKEN
git push origin --force --all
cd ..
http -b --follow GET $DOMAIN/api/v4/projects/$PROJECT_ID?statistics=true Private-Token:$TOKEN | jq . | grep -E 'path_with_namespace|http_url_to_repo|size|commit_count' | awk '{print $1, $2}' | tee -a $REPOSITORY_NAME-purge-history.json

diff $REPOSITORY_NAME-before.json $REPOSITORY_NAME-purge-history.json | tee -a $REPOSITORY_NAME-diff.txt

