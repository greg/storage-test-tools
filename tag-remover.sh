#!/bin/bash
set -e

PROJECT_ID=
DOMAIN=gitlab.com
TAGS=( )

for i in "${TAGS[@]}"
do
  http --follow DELETE "$DOMAIN/api/v4/projects/$PROJECT_ID/repository/tags/$i" Private-Token:$TOKEN
  sleep 1
done
