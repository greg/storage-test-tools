#!/bin/bash
set -x

sudo gitlab-rake gitlab:cleanup:orphan_lfs_files DRY_RUN=false DEBUG=1
sudo gitlab-rake gitlab:cleanup:project_uploads DRY_RUN=false DEBUG=1
sudo gitlab-rake gitlab:cleanup:orphan_job_artifact_files DRY_RUN=false LIMIT=10000 DEBUG=1
sudo gitlab-rake cache:clear
## un-comment if using remote uploads
# sudo gitlab-rake gitlab:cleanup:remote_upload_files DRY_RUN=false DEBUG=1
## un-comment for registry garbage collection
# sudo gitlab-ctl registry-garbage-collect
