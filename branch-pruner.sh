#!/bin/bash
set -e

PROJECT_ID=
DOMAIN=gitlab.com
BRANCH=( )

for i in "${BRANCH[@]}"
do
  http --follow DELETE "$DOMAIN/api/v4/projects/$PROJECT_ID/repository/branches/$i/" Private-Token:$TOKEN
  sleep 1
done
