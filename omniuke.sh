#!/bin/bash
# set -ex

sudo gitlab-ctl stop
sudo gitlab-ctl cleanse &&
# ubuntu/debian
sudo apt purge gitlab-ee -y &&
# centos/rhel use:
# sudo yum remove gitlab-ee
sudo rm -rf /opt/gitlab
sudo rm -rf /etc/gitlab
sudo rm -rf /var/opt/gitlab
# centos/rhel only:
sudo rm -rf /etc/sysctl.d/*gitlab*
echo "GitLab successfully removed."
