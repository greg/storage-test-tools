#!/bin/bash
PATH=
URL=
REMOTES=( )


for i in "${REMOTES[@]}"
do
  cd $PATH
  git init
  git remote remove origin
  git add .
  git remote add origin git@$URL/$i.git
  git commit -m "initial commit"
  git push -u origin master
done
