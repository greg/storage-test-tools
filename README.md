# storage-test-tools

postgresql - `project_statistics`

```sql
SELECT * FROM project_statistics;`

SELECT project_id, commit_count, storage_size, repository_size, lfs_objects_size FROM project_statistics;

UPDATE project_statistics SET storage_size = 1 WHERE project_id = 1;
```

`du`

```bash
cd /var/opt/gitlab/git-data/repositories/@hashed/
watch -d -c du -shc *
```

`logs`

```bash
# sidekiq
tail -f /var/log/gitlab/sidekiq/current | grep -Evi "sendmail|geo|elastic|mirror" | jq .

# see when project statistics are updated
tail -f -n 0 /var/log/gitlab/gitlab-rails/application.log

# gitaly
tail -f -n 0 /var/log/gitlab/gitaly/current | jq .
```

API (using httpie or curl)

```bash
# unprotect branch
# https://docs.gitlab.com/ee/api/branches.html#unprotect-repository-branch
# DELETE /projects/:id/protected_branches/:name

httpie --follow -b DELETE gitlab.com/api/v4/projects/:id/protected_branches/master Private-Token:$TOKEN
curl --request DELETE --header "PRIVATE-TOKEN:$TOKEN" "https://gitlab.com/api/v4/projects/:id/protected_branches/master"

# get storage statistics

# Project - https://docs.gitlab.com/ee/api/projects.html#get-single-project
http -b --follow GET gitlab.com/api/v4/projects/:id?statistics=true Private-Token:$TOKEN | grep -E "url_to_repo|_size"
curl --header "Private-Token:$TOKEN" "https://gitlab.com/api/v4/projects/:id?statistics=true" | grep -E "url_to_repo|_size"

# File - https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
http -b --follow GET gitlab.com/api/v4/projects/:id/repository/files/:file_path Private-Token:$TOKEN
```
