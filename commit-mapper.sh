#!/bin/bash

git clone --bare --mirror project.bundle
cd project.git

# git filter-repo --path README.md
# git filter-repo --blob-callback '
# if blob.data.startswith(b"version https://git-lfs.github.com/spec/v1"):
#   size_in_bytes = int.from_bytes(blob.data[124:], byteorder="big")
#   if size_in_bytes > 10*1000:
#     blob.skip()
# '

# git filter-repo --path big.txt --invert-path
# git filter-repo --strip-blobs-bigger-than 1M
# git filter-repo --path README.md
